<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;

$router->group(["middleware" => "authMiddleware"], function () use ($router) {
    Route::get("/api/secure/validate", "AuthController@validateToken");

    Route::get("/api/presence/list", "PresenceController@getPresenceList");
    Route::get("/api/presence/list/{lesson}", "PresenceController@getPresenceListByLesson");
    Route::post("/api/presence/save", "PresenceController@save");

    Route::get("/api/lessons/{date}", "LessonController@getLessonListByDate");
    Route::get("/api/lesson/{lesson}", "LessonController@getLesson");
});

Route::post("/api/secure/authenticate", "AuthController@authenticate");