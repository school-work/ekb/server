<?php

namespace App\Helper;

/**
 * Class TokenHelper
 * @package App\Helper
 * @author Lars Riße
 */
class TokenHelper
{

    /**
     * Function to generate a simple hash token.
     * @return string
     */
    public static function generate()
    {
        return hash("whirlpool", md5(time()));
    }
}