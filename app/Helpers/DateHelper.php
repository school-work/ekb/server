<?php

namespace App\Helpers;

/**
 * Class DateHelper
 * @package App\Helpers
 * @author Lars Riße & Patrick Helbig
 */
class DateHelper
{
    private static $schoolHours = array(
        1 => array(
            "from" => "07:45",
            "until" => "08:30"
        ),
        2 => array(
            "from" => "08:30",
            "until" => "09:15"
        ),
        3 => array(
            "from" => "09:40",
            "until" => "10:25"
        ),
        4 => array(
            "from" => "10:25",
            "until" => "11:10"
        ),
        5 => array(
            "from" => "11:30",
            "until" => "12:15"
        ),
        6 => array(
            "from" => "12:15",
            "until" => "13:15"
        ),
        7 => array(
            "from" => "13:15",
            "until" => "14:00"
        ),
        8 => array(
            "from" => "14:00",
            "until" => "14:45"
        )
    );

    public static function getTrueHours($from, $until)
    {
        $from = str_replace(":00.0000000", "", $from);
        $until = str_replace(":00.0000000", "", $until);

        $from = array_search($from, array_column(DateHelper::$schoolHours, "from"));
        $until = array_search($until, array_column(DateHelper::$schoolHours, "until"));

        return $until - $from + 1;
    }
}