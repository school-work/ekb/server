<?php

namespace App\Http\Controllers;

use App\Helper\FormatHelper;
use DB;
use Illuminate\Http\Request;

/**
 * Class LessonController
 * @package App\Http\Controllers
 * @author Lars Riße & Patrick Helbig
 */
class LessonController extends Controller
{
    public function getLessonListByDate($date, Request $request)
    {
        $shortName = utf8_encode($request->header("auth_name"));

        if (!empty($date) && !empty($shortName)) {
            $lessons = DB::select("EXEC dbo.getLessonsByDate ?,?", [$shortName, $date]);

            foreach ($lessons as $lesson) {
                $classTeacher = DB::table("Klasse")->where("ID", $lesson->KlassenID)->where("KlassenlehrerID", $shortName)->first();
                $courseLeader = DB::table("Klasse")->join("BildungsgangLeiter", "Klasse.BildungsgangID", "=", "BildungsgangLeiter.BildungsgangID")->where("Klasse.ID", $lesson->KlassenID)->where("BildungsgangLeiter.LeiterID", $shortName)->first();

                if (!empty($classTeacher)) {
                    $lesson->classTeacher = true;
                } else {
                    $lesson->classTeacher = false;
                }

                if (!empty($courseLeader)) {
                    $lesson->courseLeader = true;
                } else {
                    $lesson->courseLeader = false;
                }
            }

            return FormatHelper::formatData($lessons);
        } else {
            return FormatHelper::formatData(array("error" => "missing-fields"), false, 400);
        }
    }

    public function getLesson($lesson, Request $request)
    {
        $shortName = utf8_encode($request->header("auth_name"));

        if (!empty($lesson)) {
            $result = DB::table("Stunde")->where("ID", $lesson)->first();
            $classTeacher = DB::table("Klasse")->where("ID", $result->KlassenID)->where("KlassenlehrerID", $shortName)->first();
            $courseLeader = DB::table("Klasse")->join("BildungsgangLeiter", "Klasse.BildungsgangID", "=", "BildungsgangLeiter.BildungsgangID")->where("Klasse.ID", $result->KlassenID)->where("BildungsgangLeiter.LeiterID", $shortName)->first();

            if ($result->LehrerID != $shortName && empty($classTeacher) && empty($courseLeader)) {
                return FormatHelper::formatData(array("error" => "not-allowed"), false, 401);
            }

            if (!empty($classTeacher)) {
                $result->classTeacher = true;
            } else {
                $result->classTeacher = false;
            }

            if (!empty($courseLeader)) {
                $result->courseLeader = true;
            } else {
                $result->courseLeader = false;
            }

            return FormatHelper::formatData($result);
        } else {
            return FormatHelper::formatData(array("error" => "missing-fields"), false, 400);
        }
    }
}