<?php

namespace App\Http\Controllers;

use App\Helper\FormatHelper;
use App\Helper\TokenHelper;
use App\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller;

/**
 * Class AuthController
 * @package App\Http\Controllers
 * @author Lars Riße
 */
class AuthController extends Controller
{
    public function authenticate(Request $request)
    {
        $shortName = $request->input("shortName");
        $password = $request->input("password");

        $exist = DB::select("EXEC dbo.CheckTeacherPassword ?, ?", [
            $shortName,
            $password
        ]);

        if (empty($exist)) {
            return FormatHelper::formatData(array("error" => "wrong"), false, 401);
        } else {
            $okay = $exist[0]->return == 1;

            if ($okay) {
                $date = new \DateTime();
                $date->modify("+15 minutes");

                $data = array(
                    "LehrerID" => $shortName,
                    "Token" => TokenHelper::generate(),
                    "Timestamp" => $date->format("Y-d-m H:i:s")
                );
                DB::table("session")->insert($data);

                return FormatHelper::formatData($data);
            } else {
                return FormatHelper::formatData(array("error" => "wrong"), false, 401);
            }
        }
    }

    public function validateToken()
    {
        return FormatHelper::formatData(array("success"));
    }
}