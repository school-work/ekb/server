<?php

namespace App\Http\Controllers;

use App\Helper\FormatHelper;
use App\Helpers\DateHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class PresenceController
 * @package App\Http\Controllers
 * @author Lars Riße & Patrick Helbig
 */
class PresenceController extends Controller
{
    public function getPresenceList(Request $request)
    {
        $shortName = utf8_encode($request->header("auth_name"));
        return FormatHelper::formatData($this->queryPresenceList($shortName));
    }

    public function getPresenceListByLesson($lesson, Request $request)
    {
        $shortName = utf8_encode($request->header("auth_name"));
        return FormatHelper::formatData($this->queryPresenceList($shortName, $lesson));
    }

    public function save(Request $request)
    {
        $shortName = $request->header("auth_name");

        foreach ($request->all() as $item) {
            $result = DB::table("Stunde")->where("ID", $item["StundenID"])->first();

            if (!empty($result)) {
                if ($item["LehrerID"] != utf8_encode($shortName) || $result->LehrerID != utf8_encode($shortName) || $result->LehrerID != $item["LehrerID"]) {
                    return FormatHelper::formatData(array("error" => "not-allowed"), false, 401);
                }

                if ($item["Fehlzeit"] == null || $item["Fehlzeit"] > $item["maxTime"] || $item["Fehlzeit"] < 0) {
                    return FormatHelper::formatData(array("error" => "fehlzeit-not-valid"), false, 400);
                }
            } else {
                return FormatHelper::formatData(array("error" => "lesson-not-found"), false, 400);
            }
        }

        $array = array();

        foreach ($request->all() as $item) {
            DB::update("EXEC dbo.setPresence ?,?,?,?,?", [$item["StundenID"], $item["LehrerID"], $item["SchuelerID"], $item["Fehlzeit"], $item["Entschuldigt"]]);
        }

        return FormatHelper::formatData($array);
    }

    private function queryPresenceList($shortName, $lessonId = null)
    {
        $presenceList = [];
        if ($lessonId != null) {
            $lesson = DB::table("Stunde")->where("ID", $lessonId)->first();
        } else {
            $lesson = DB::select("EXEC dbo.getCurrentLessonInformation ?", [$shortName]);
        }

        if (!empty($lesson)) {
            if ($lessonId == null) {
                $lesson = $lesson[0];
            }

            $classTeacher = DB::table("Klasse")->where("ID", $lesson->KlassenID)->where("KlassenlehrerID", $shortName)->first();
            $courseLeader = DB::table("Klasse")->join("BildungsgangLeiter", "Klasse.BildungsgangID", "=", "BildungsgangLeiter.BildungsgangID")->where("Klasse.ID", $lesson->KlassenID)->where("BildungsgangLeiter.LeiterID", $shortName)->first();

            if ($lessonId != null) {
                $presenceList = DB::select("EXEC dbo.getPresenceListByLessonId ?", [$lessonId]);
            } else {
                $presenceList = DB::select("EXEC dbo.getPresenseList ?", [$shortName]);
            }

            if (empty($presenceList) || count($presenceList) == 0) {
                DB::insert("INSERT INTO Anwesenheit (StundenID, SchuelerID, LehrerID, Fehlzeit, Timestamp, Entschuldigt, Geloescht) SELECT Stunde.ID, SchuelerKlasse.SchuelerID, Stunde.LehrerID, 0, CURRENT_TIMESTAMP, 0, 0 FROM Stunde JOIN SchuelerKlasse ON SchuelerKlasse.KlasseID = Stunde.KlassenID WHERE Stunde.ID = $lesson->ID");

                return $this->queryPresenceList($shortName, $lessonId);
            } else {
                foreach ($presenceList as $item) {
                    if (!empty($classTeacher)) {
                        $item->classTeacher = true;
                    } else {
                        $item->classTeacher = false;
                    }

                    if (!empty($courseLeader)) {
                        $item->courseLeader = true;
                    } else {
                        $item->courseLeader = false;
                    }

                    $item->maxTime = DateHelper::getTrueHours($lesson->VonUhrzeit, $lesson->BisUhrzeit) * 45;
                }
            }
        }

        return $presenceList;
    }
}