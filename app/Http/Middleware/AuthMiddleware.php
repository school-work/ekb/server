<?php

namespace App\Http\Middleware;


use App\Helper\FormatHelper;
use Closure;
use Illuminate\Http\Request;

class AuthMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  Request $request
     * @param  Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header("auth_token");
        $shortName = $request->header("auth_name");

        if ($token == null || $shortName == null) {
            return FormatHelper::formatData(array("error" => "no-token"), false, 401);
        }

        if ($this->validateToken($token, $shortName) === false) {
            return FormatHelper::formatData(array("error" => "token-not-valid"), false, 401);
        }

        $this->renewToken($token, $shortName);

        return $next($request);
    }

    private function validateToken($token, $shortName)
    {
        $obj = \DB::table("session")->where("Token", $token)->where("LehrerId", utf8_encode($shortName))->first();
        if ($obj == null) {
            return false;
        } else {
            $dateTime = \DateTime::createFromFormat("Y-m-d H:i:s.u", $obj->Timestamp);
            if (time() > $dateTime->getTimestamp()) {
                return false;
            }

            return true;
        }
    }

    private function renewToken($token, $shortName)
    {
        $obj = \DB::table("session")->where("Token", $token)->where("LehrerId", utf8_encode($shortName))->first();

        if ($obj != null) {
            $date = new \DateTime();
            $date->modify("+15 minutes");

            $data = array("Timestamp" => $date->format("Y-d-m H:i:s"));
            \DB::table("session")->where("Token", $token)->where("LehrerId", utf8_encode($shortName))->update($data);
        }

    }
}
